/*
 * @file node.cpp
 * Created on: 16/07/2014
 * 16/07/14: Initial version
 *
 *  @author Alberto Hata
 *
 */

#include <velodyne_curb_detector/detector/velodyne_curb_detector.hpp>

using namespace velodyne_curb_detector;

int main(int argc, char** argv) {
  ros::init(argc, argv, "velodyne_curb_detector");

  VelodyneCurbDetector velodyneCurbDetector;

  ros::spin();

  return 0;
}
