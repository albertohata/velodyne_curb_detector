/*
 * @file velodyne_curb_detector.cpp
 * Created on: 16/07/2014
 * 16/07/14: Initial version
 * 02/12/18: Code documentation
 *
 * @author Alberto Hata
 *
 */

#include <velodyne_curb_detector/detector/velodyne_curb_detector.hpp>

namespace velodyne_curb_detector {
  
  /**
   * Constructor of the velodyne_curb_detector.
   *
   */
  VelodyneCurbDetector::VelodyneCurbDetector() {

    // Set curb detector parameters
    nh_.param("range_max", this->range_max_, 30.0);
    nh_.param("range_min", this->range_min_, 3.0);
    nh_.param("ring_max", this->ring_max_, 22);
    nh_.param("height_max", this->height_max_, 0.3);
    nh_.param("x_max", this->x_max_, 30.0);
    nh_.param("y_max", this->y_max_, 6.0);
    nh_.param<string>("velodyne_frame", this->velodyne_frame_, "/velodyne");
    nh_.param<string>("base_frame", this->base_frame_, "/base_footprint");

    // Detector default parameters
    curb_max_param_ = 1.02254;
    curb_min_param_ = 0.434093;
    slope_param_ = 0.077056;
    reg_dist_param_ = 0.243055;

    // Subscribers
    pcl2_sub_ = nh_.subscribe("/velodyne_points", 10, &VelodyneCurbDetector::velodyneCallback, this, ros::TransportHints().tcpNoDelay(true));

    // Publishers
    pcl2_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/curb_candidates", 10, false);
    marker_pub_ = nh_.advertise<visualization_msgs::Marker>("/curb_marker", 10, false);
    curbParam_pub_ = nh_.advertise<velodyne_curb_detector::CurbParams>("/curb_params", 10, false);
    pcl_curb_filt_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/curb_candidates_filtered", 10, false);

    // Velodyne height calculated using TF (calculated by tranforming /base_footprint frame to /velodyne frame)
    tf::StampedTransform stamped_tf;

    try {
      tf_listener_.waitForTransform(this->base_frame_, this->velodyne_frame_, ros::Time(), ros::Duration(10.0));
      tf_listener_.lookupTransform(this->base_frame_, this->velodyne_frame_, ros::Time(), stamped_tf);
      tf::Vector3 v = stamped_tf.getOrigin();
      this->velodyne_height_ = v.getZ();
      ROS_DEBUG("velodyne height: %f", this->velodyne_height_);
    }
    catch (tf::TransformException e) {
      ROS_WARN("%s", e.what());
      // Uncomment if using Carina I
      // velodyne_height = 1.19;
      // Mounting height in Carina II
      this->velodyne_height_ = 2.3;
      ROS_WARN("Using default velodyne height: %f", this->velodyne_height_);
    }

    VelodyneCurbDetectorFilter *filter = new VelodyneCurbDetectorFilter(this->ring_max_, this->x_max_, this->y_max_, this->range_min_, this->range_max_, this->height_max_, this->curb_min_param_, this->curb_max_param_, this->slope_param_, this->reg_dist_param_, this->velodyne_height_, this->velodyne_frame_, this->base_frame_);

    this->detectorFilter_ = filter;

  }


  /**
   * Class destructor.
   *
   */
  VelodyneCurbDetector::~VelodyneCurbDetector() {

  }


  /**
   * Method to publish a single point.
   *
   * @param[in] stamp Stamp of the published point
   * @param[in] frame_id Frame id of the published point
   *
   */
  void VelodyneCurbDetector::publishSinglePoint(uint64_t stamp, string frame_id) {
    PclXYZType::Ptr cloud(new PclXYZType);

    cloud->height = 1;
    cloud->header.stamp = stamp;
    cloud->header.frame_id = frame_id;

    cloud->points.push_back(pcl::PointXYZ(0, 0, 0));
    cloud->width++;

    this->pcl_curb_filt_pub_.publish(cloud);
  }


  /**
   * Method of the velodyne sensor message callback.
   *
   * @param[in] pcl2Msg Velodyne cloud message obtained from the sensor
   *
   */
  void VelodyneCurbDetector::velodyneCallback(const VelodynePclType::ConstPtr& pcl2Msg) {
    
    PclXYZType::Ptr cloudPcl2Out(new PclXYZType());
    VelodynePclType pcl2Transformed;
    PclXYZType::Ptr curb_points_filt(new PclXYZType());

    // Terminate if received a empty cloud
    if (pcl2Msg->points.empty()) {
      return;
    }

    // Set stamp and base frame id
    cloudPcl2Out->height = 1;
    cloudPcl2Out->header.stamp = pcl2Msg->header.stamp;
    cloudPcl2Out->header.frame_id = velodyne_frame_;

    pcl2Transformed.height = 1;
    pcl2Transformed.header.stamp = pcl2Msg->header.stamp;
    pcl2Transformed.header.frame_id = velodyne_frame_;
    
    curb_points_filt->height = 1;
    curb_points_filt->header.stamp = pcl2Msg->header.stamp;
    curb_points_filt->header.frame_id = velodyne_frame_;

    this->curb_params_.header.stamp = pcl_conversions::fromPCL(pcl2Msg->header).stamp;
    this->curb_params_.header.frame_id = velodyne_frame_;
    this->detectorFilter_->clearVariables(pcl2Msg->header.stamp, this->base_frame_);

    // Transform  velodyne pointcloud to /base_footprint frame
    try {
      this->tf_listener_.waitForTransform(this->base_frame_, "/velodyne", pcl_conversions::fromPCL(pcl2Msg->header).stamp, ros::Duration(0.5));
      this->tf_listener_.lookupTransform(this->base_frame_, "/velodyne", pcl_conversions::fromPCL(pcl2Msg->header).stamp, this->transform_);
      pcl_ros::transformPointCloud<velodyne_pointcloud::PointXYZIR>(*pcl2Msg, pcl2Transformed, this->transform_);
    }
    catch (tf::TransformException e){
      ROS_DEBUG("%s", e.what());
      copyPointCloud(*pcl2Msg, pcl2Transformed);
    }

    // Analyse the difference of distance of points between consecutive rings
    this->detectorFilter_->ringCompressionFilter(pcl2Transformed, *cloudPcl2Out);

    // End callback if no curb was detected
    if (cloudPcl2Out->points.size() == 0) {
      publishSinglePoint(pcl2Msg->header.stamp, this->base_frame_);
      return;
    }

    // Remove false positives
    this->detectorFilter_->regressionFilter(*cloudPcl2Out, *curb_points_filt);

    // End callback if any curb was detected
    if (curb_points_filt->points.size() == 0) {
      publishSinglePoint(pcl2Msg->header.stamp, this->base_frame_);
      return;
    }

    this->detectorFilter_->getRegressionParameters(this->curb_params_);

    // Publish the curb segment
    this->detectorFilter_->getRegressionCurves(this->curb_line_A_, this->curb_line_B_);
    this->marker_pub_.publish(this->curb_line_A_);
    this->marker_pub_.publish(this->curb_line_B_);

    // Publish detected curb points
    this->pcl2_pub_.publish(cloudPcl2Out);
    this->pcl_curb_filt_pub_.publish(curb_points_filt);
    this->curbParam_pub_.publish(this->curb_params_);
  }
}
