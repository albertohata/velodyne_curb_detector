/*
 * @file filter.cpp
 * Implementation of the filter class with the ring compression, gradient and LTS filters.
 * Created on: 20/07/2014
 * 16/07/14: Initial version
 *
 * @author: Alberto Hata
 *
 */

#include <velodyne_curb_detector/filter/filter.hpp>

namespace velodyne_curb_detector {

  /**
   * @brief Class to filter the curb candidates points.
   *
   * This uses the ring compression and LTS regression to detect curbs.
   */
    VelodyneCurbDetectorFilter::VelodyneCurbDetectorFilter(int ring_max, double x_max, double y_max,
                                                           double range_min, double range_max, double height_max,
                                                           double curb_min_param, double curb_max_param, double slope_param,
                                                           double reg_dist_param, double velodyne_height, 
                                                           string velodyne_frame, string base_frame) : 
                                                           ring_max_(ring_max), x_max_(x_max), y_max_(y_max), 
                                                           range_min_(range_min), range_max_(range_max), height_max_(height_max), 
                                                           curb_min_param_(curb_min_param), curb_max_param_(curb_max_param), slope_param_(slope_param), 
                                                           reg_dist_param_(reg_dist_param), velodyne_height_(velodyne_height), velodyne_frame_(velodyne_frame), base_frame_(base_frame) {


    // Build the default grid content (without content)
    this->defaultGrid_.assign(this->ring_max_, vector<VRay>());

    for (int i=0; i<this->ring_max_; i++) {

      // 1. Compute grid number of rows, considering the cell size
      // The row size is defined as the ring circunference length divided by the cell size 
      this->defaultGrid_[i].assign(2 * M_PI * fabs(this->velodyne_height_ * tan(M_PI_2 - M_PI/180.0 * (23-i) * 4/3.0)) / 0.15, VRay()); // cell size: 0.15 m

      // 2. Compute the cell angular size of each ring.
      this->cell_sector_size_[i] = 2 * M_PI / (double)this->defaultGrid_[i].size();

      // 3. Compute detector threshold upper bound (maximum ring difference threshold)
      // Default consecutive ring distance divided by a factor
      this->curb_max_threshold_[i] = this->velodyne_height_ * (tan(M_PI_2 - M_PI/180.0 * ((22-i) * 4/3.0)) - tan(M_PI_2 - M_PI/180.0 * (23-i) * 4/3.0)) * this->curb_max_param_;

      // 4. Compute detector threshold lower bound
      // Corresponds to curb_max_threshold - 70%
      this->curb_min_threshold_[i] = this->curb_max_threshold_[i] * this->curb_min_param_;
    }


    // Set marker default values
    this->curb_line_A_.header.frame_id = this->curb_line_B_.header.frame_id = base_frame_;
    this->curb_line_A_.ns = this->curb_line_B_.ns = "curb_lines";
    this->curb_line_A_.action = this->curb_line_B_.action = visualization_msgs::Marker::ADD;
    this->curb_line_A_.pose.orientation.w = this->curb_line_B_.pose.orientation.w = 1.0;
    this->curb_line_A_.type = this->curb_line_B_.type = visualization_msgs::Marker::LINE_STRIP;
    this->curb_line_A_.id = 0;
    this->curb_line_B_.id = 1;
    this->curb_line_A_.color.b = this->curb_line_B_.color.b = 1.0;
    this->curb_line_A_.color.a = this->curb_line_B_.color.a = 0.5;
    this->curb_line_A_.scale.x = this->curb_line_B_.scale.x = 0.2;

    // Set fitter variables (using 2nd order polynomial)
    this->f_ = new TFormula("pol2", "pol2");
    this->lf_A_ = new TLinearFitter(this->f_);
    this->lf_B_ = new TLinearFitter(this->f_);

  }


  /**
   * Class destructor.
   *
   */
  VelodyneCurbDetectorFilter::~VelodyneCurbDetectorFilter() {

  }


  /**
   * Method to calculate the gradient based on two points.
   *
   * @param[in]  p1 First point 
   * @param[in]  p2 Second point 
   * @return  Gradient value
   *
   */
  template<typename PointT>
  double VelodyneCurbDetectorFilter::get3DGradient(PointT p1, PointT p2) {
    double d_height, x_dist, y_dist, d_dist;

    d_height = p1.z - p2.z;
    y_dist = p1.y - p2.y;
    x_dist = p1.x - p2.x;
    d_dist = sqrt(y_dist*y_dist + x_dist*x_dist);

    return (d_dist != 0) ? d_height/d_dist : 0;
  }


  /**
   * Method to clear the curb detector parameters.
   *
   * @param[in]  stamp Stamp value to set in the header
   * @param[in]  frame_id Frame id to set in the header
   *
   */
  void VelodyneCurbDetectorFilter::clearVariables(uint64_t stamp, string frame_id) {
    this->lf_A_->ClearPoints();
    this->lf_B_->ClearPoints();

    this->curb_points_[0].clear();
    this->curb_points_[0].height = 1;
    this->curb_points_[1].clear();
    this->curb_points_[1].height = 1;
    this->curb_points_filt_.clear();
    this->curb_points_filt_.height = 1;

    this->curb_points_[0].header.stamp = stamp;
    this->curb_points_[0].header.frame_id = frame_id;
    this->curb_points_[1].header.stamp = stamp;
    this->curb_points_[1].header.frame_id = frame_id;
    this->curb_points_filt_.header.stamp = stamp;
    this->curb_points_filt_.header.frame_id = frame_id;
  }


  /**
   * Method to generate circular grid.
   *
   * @param[in]  pcl Point cloud data to generate grid
   * @param[out]  grid Generated grid
   *
   */
  void VelodyneCurbDetectorFilter::pcl2CircularGrid(VelodynePclType pcl, CircularGridType &grid) {

    grid = this->defaultGrid_;
    
    for (size_t i = 0; i < pcl.points.size(); ++i) {
      double x = pcl.points[i].x;
      double y = pcl.points[i].y;
      double z = pcl.points[i].z;
      int ring = pcl.points[i].ring;
      float intensity = pcl.points[i].intensity;

      double range_sqrt = sqrt(y*y + x*x);

      if( z < height_max_
          && fabs(x) < x_max_
          && fabs(y) < y_max_
          && ring < ring_max_
          && range_sqrt < range_max_
          && range_sqrt > range_min_
        ) {

        double angle = atan2(x, -y);

        angle += (angle<0)?2*M_PI:((angle>2*M_PI)?-2*M_PI:0);

        int index = angle / cell_sector_size_[ring];

        grid[ring][index].number_points++;
        grid[ring][index].range     += range_sqrt;
        grid[ring][index].x         += x;
        grid[ring][index].y         += y;
        grid[ring][index].z         += z;
        grid[ring][index].ring       = ring;
        grid[ring][index].intensity += intensity;
        
      }
    }

    // Store the velodyne data in a grid.
    for (int i=0; i<ring_max_; ++i) {
      for (unsigned int j=0; j<grid[i].size(); ++j) {
        if (grid[i][j].number_points > 0) {

          double number_points = (double)grid[i][j].number_points;
          grid[i][j].x            /= number_points;
          grid[i][j].y            /= number_points;
          grid[i][j].z            /= number_points;
          grid[i][j].range        /= number_points;
          grid[i][j].intensity    /= number_points;
          grid[i][j].number_points = 1;

        }
      }
    }

  }


  /**
   * Method to filter points using ring compression.
   *
   * @param[in]  pclIn Input point cloud data 
   * @param[out]  pclOut Output point cloud resulted from ring compression
   *
   */
//  template<typename PointT>
//  void VelodyneCurbDetectorFilter::ringCompressionFilter(VelodynePclType pclIn, PointCloud<PointT> &pclOut) {
  void VelodyneCurbDetectorFilter::ringCompressionFilter(VelodynePclType pclIn, PclXYZType &pclOut) {
    double gradient;
    CircularGridType grid;

    if (pclIn.points.size() == 0) {
      return;
    }

    pcl2CircularGrid(pclIn, grid);

    for (int i=2; i<this->ring_max_-1; ++i) {

      for (unsigned int j=0; j<grid[i].size(); ++j) {

        // Check if current and adjacent cells are not empty and it is not the first or last laser index
        if ((j!=0 && j!=grid[i].size()-1) &&
            (grid[i][j].number_points > 0 && grid[i][j+1].number_points > 0 && grid[i][j-1].number_points > 0)) {
          
          gradient = fabs(get3DGradient(PointXYZ(grid[i][j+1].x, grid[i][j+1].y, grid[i][j+1].z), 
                                        PointXYZ(grid[i][j-1].x, grid[i][j-1].y, grid[i][j-1].z))); // Compute slope
        }
        else {
          continue;
        }

        // Ensure that the current cell contains points and the adjacent points are situated on a slope
        if (gradient !=0 && gradient > this->slope_param_) {

          // Compute respective index j for ring i-1 and ring i+1
          int k1 = j*this->cell_sector_size_[i]/this->cell_sector_size_[i-1];
          int k2 = j*this->cell_sector_size_[i]/this->cell_sector_size_[i+1];

          // Compare with previous ring data
          if ((grid[i-1][k1].number_points > 0 && 
               fabs(grid[i][j].range - grid[i-1][k1].range) < this->curb_max_threshold_[i] && 
               fabs(grid[i][j].range - grid[i-1][k1].range) > this->curb_min_threshold_[i]) ||
              (grid[i+1][k2].number_points > 0 && fabs(grid[i+1][k2].range - grid[i][j].range) < this->curb_max_threshold_[i+1] && fabs(grid[i+1][k2].range - grid[i][j].range) > this->curb_min_threshold_[i+1])) {

            pclOut.points.push_back(pcl::PointXYZ(grid[i][j].x, grid[i][j].y, grid[i][j].z));
            pclOut.width++;

          }
        }
      }
    }
  }
  

  /**
   * Method to filter the curb points using the LTS regression.
   *
   * @param[in]  pclIn Input point cloud data 
   * @param[out]  pclOut Output point cloud resulted from LTS regression filter
   *
   */
//  template<typename PointT>
//  void VelodyneCurbDetectorFilter::regressionFilter(PointCloud<PointT> pclIn, PointCloud<PointT> &pclOut) {
  void VelodyneCurbDetectorFilter::regressionFilter(PclXYZType pclIn, PclXYZType &pclOut) {
      
    double x, y;
    PclXYZType pclFiltered;
    TVectorD paramListA, paramListB;

    this->lf_A_->ClearPoints();
    this->lf_B_->ClearPoints();

    // Separate compression result into left and right points
    for (size_t i=0; i<pclIn.points.size(); i++) {
      Double_t vx[1] = {pclIn.points[i].x};
      (pclIn.points[i].y < 0) ? this->lf_A_->AddPoint(vx, pclIn.points[i].y, 1) : 
                                this->lf_B_->AddPoint(vx, pclIn.points[i].y, 1);
    }

    // Compute LTS regression based on left and right curb points
    computeRegression(*(this->lf_A_), *(this->lf_B_));

    pclFiltered.height = 1;
    pclFiltered.header.stamp = pclIn.header.stamp;
    pclFiltered.header.frame_id = pclIn.header.frame_id;

    for (pcl::PointCloud<pcl::PointXYZ>::iterator it=pclIn.points.begin(); it!=pclIn.points.end(); it++) {
      x = it->x;
      y = (it->y < 0 && this->params_A_.GetNoElements() > 0) ? this->params_A_(0) + this->params_A_(1)*x + this->params_A_(2)*x*x : 
         ((it->y > 0 && this->params_B_.GetNoElements() > 0) ? this->params_B_(0) + this->params_B_(1)*x + this->params_B_(2)*x*x : -it->y);

      if (fabs(it->y - y) < this->reg_dist_param_) {
        pclFiltered.push_back(*it);
      }
    }

    copyPointCloud(pclFiltered, pclOut);
  }


  /**
   * Method to calculate the curb curve points using the LTS parameters.
   *
   * @param[in]  curb_line_A Parameters of the right curb
   * @param[in]  curb_line_B Parameters of the left curb
   *
   */
  void VelodyneCurbDetectorFilter::getRegressionCurves(visualization_msgs::Marker &curb_line_A, visualization_msgs::Marker &curb_line_B) {
    geometry_msgs::Point p;

    this->curb_line_A_.points.clear();
    this->curb_line_B_.points.clear();

    for (int i=-30; i<30; i=i+2) {
      p.x = i;

      if (this->params_A_.GetNoElements() > 0) {
        p.y = this->params_A_(0) + this->params_A_(1)*i + this->params_A_(2)*i*i;
        this->curb_line_A_.points.push_back(p);    
      }

      if (this->params_B_.GetNoElements() > 0) {
        p.y = this->params_B_(0) + this->params_B_(1)*i + this->params_B_(2)*i*i;
        this->curb_line_B_.points.push_back(p);
      }
    }

    curb_line_A = this->curb_line_A_;
    curb_line_B = this->curb_line_B_;

  }


  /**
   * Method to get the LTS parameters.
   *
   * param[out]  curb_params Curb curve parameters obtained from LTS regression
   *
   */
  void VelodyneCurbDetectorFilter::getRegressionParameters(CurbParams &curb_params) {

    curb_params.rightCurbParam.clear();
    curb_params.leftCurbParam.clear();

    if (this->params_A_.GetNoElements() > 0) {
      curb_params.rightCurbParam.push_back(this->params_A_(0));
      curb_params.rightCurbParam.push_back(this->params_A_(1));
      curb_params.rightCurbParam.push_back(this->params_A_(2));
    }

    if (this->params_B_.GetNoElements() > 0) {
      curb_params.leftCurbParam.push_back(this->params_B_(0));
      curb_params.leftCurbParam.push_back(this->params_B_(1));
      curb_params.leftCurbParam.push_back(this->params_B_(2));
    }

  }


  /**
   * Method to compute LTS regression.
   *
   * @param[in]  fitterA Set of points of the left curb to be fitted
   * @param[in]  fitterB Set of points of the right curb to be fitted
   *
   */
  void VelodyneCurbDetectorFilter::computeRegression(TLinearFitter fitterA, TLinearFitter fitterB)  {
    TVectorD paramListA, paramListB;

    paramListA.Clear("");
    paramListB.Clear("");

    // Add previous fittted curve points to the fitter (add "temporal" information)
    for (int i=-10; i<10; i=i+2) {
      double y;
      Double_t vx[1] = {(double)i};

      if (fitterA.GetNpoints() > 8 && this->params_A_.GetNoElements() > 0) {
        y = this->params_A_(0) + this->params_A_(1)*i + this->params_A_(2)*i*i;
        fitterA.AddPoint(vx, y, 0.5);
      }

      if (fitterB.GetNpoints() > 8 && this->params_B_.GetNoElements() > 0) {
        y = this->params_B_(0) + this->params_B_(1)*i + this->params_B_(2)*i*i;
        fitterB.AddPoint(vx, y, 0.5);
      }
    }
    
    TVectorD params_A_aux = this->params_A_, params_B_aux = this->params_B_;

    if (fitterA.GetNpoints() > 9) {
      fitterA.EvalRobust();
      //fitterA.Eval();
      fitterA.GetParameters(paramListA);
    }
    if (fitterB.GetNpoints() > 9) {
      fitterB.EvalRobust();
      //fitterB.Eval();
      fitterB.GetParameters(paramListB);
    }

    // Use previously calculated parameters to obtain a more smooth change
    if (paramListA.GetNoElements() > 0 && params_A_aux.GetNoElements() > 0) {
      paramListA(0) = paramListA(0) * 0.4 + params_A_aux(0) * 0.6;
      paramListA(1) = paramListA(1) * 0.4 + params_A_aux(1) * 0.6;
      paramListA(2) = paramListA(2) * 0.4 + params_A_aux(2) * 0.6;
    }
    if (paramListB.GetNoElements() > 0 && params_B_aux.GetNoElements() > 0) {
      paramListB(0) = paramListB(0) * 0.4 + params_B_aux(0) * 0.6;
      paramListB(1) = paramListB(1) * 0.4 + params_B_aux(1) * 0.6;
      paramListB(2) = paramListB(2) * 0.4 + params_B_aux(2) * 0.6;
    }


    if (paramListA.GetNoElements() > 0) {
      this->params_A_.Clear("");
      this->params_A_.ResizeTo(3);
      this->params_A_(0) = paramListA(0);
      this->params_A_(1) = paramListA(1);
      this->params_A_(2) = paramListA(2);
    }

    if (paramListB.GetNoElements() > 0) { 
      this->params_B_.Clear("");
      this->params_B_.ResizeTo(3);
      this->params_B_(0) = paramListB(0);
      this->params_B_(1) = paramListB(1);
      this->params_B_(2) = paramListB(2);
    }
  }

}
