# Velodyne Curb Detector Package

## 1. Description

This package provides ROS nodes to detect curbs in urban roads. The curb detector is based on the ring compression
method employed in the Sanford's Stanley autonomous vehicle using Velodyne HDL-32E lidar. In Stanley the ring compression 
was used for obstacle detection. Therefore the method was extended to make possible obtaining the points in the cloud
that corresponds to the curb.

In addition to the ring compression, the method relies on three additional filters:

- **Distance filter:** Remove points that are too far from the lidar sensor.

- **Gradient filter:** Remove points that have slope values that does not correspond to a curb shape.

- **Regression filter:** Makes use of robust regression method named Least Trimmed Squares (LTS). By using LTS regression it
is possible to remove outliers from the set of curb points.

Details of the method and application to vehicle localization can be found in the paper "Robust curb detection and vehicle localization in urban environments"
https://ieeexplore.ieee.org/abstract/document/6856405

In the following video it is possible to check the execution of the velodyne curb detector.

[![Velodyne Curb Detector](http://img.youtube.com/vi/vflYa5qhi48/0.jpg)](http://www.youtube.com/watch?v=vflYa5qhi48)

## 2. Requirements

The package was tested in ROS Kinect on Ubuntu 16.04 machine.
The curb detector was tested on Velodyne HDL-32E lidar.

## 3. Intallation

### 3.1. Install ROS Kinect on Ubuntu 16.04

- Install ROS Kinect by following the instructions in http://wiki.ros.org/kinetic/Installation/Ubuntu

- Create a rosbuild workspace by following the instructions in http://wiki.ros.org/ROS/Tutorials/rosbuild/CreateWorkspace

**Note:** As this is an old package, it uses **rosbuild workspace**, however it works properly with ROS Kinect.

**Note2:** Make sure that the ROS environment is properly set before proceeding to the next steps.

### 3.2. Install dependencies

- Install ROS velodyne-point-cloud and pcl-ros packages:
```sh
sudo apt-get install ros-kinetic-velodyne-pointcloud
sudo apt-get install ros-kinetic-pcl-ros
sudo apt-get install ros-kinetic-pointcloud-to-laserscan
```

- Install root cern library (contains the LTS regression library)
```sh
sudo apt-get install root-system
```

### 3.3. Clone and compile velodyne_curb_detector package

- Clone the velodyne_curb_detector repository inside the rosbuild workspace:

```sh
cd PATH_TO_ROS_BUILD_WORKSPACE
git clone https://albertohata@bitbucket.org/albertohata/velodyne_curb_detector.git
```

- Compile the package:

```sh
cd PATH_TO_ROS_BUILD_WORKSPACE
rosmake velodyne_curb_detector
```

## 4. Running velodyne_curb_detector package

### 4.1. Download the carina log file (rosbag)

- Download data log 1 or 2 from http://lrm.icmc.usp.br/web/index.php?n=Eng.Repositorio?userlang=en
```sh
wget http://143.107.183.119:5380/datasets/volta_ambiental_velodyne_septentrio_xsens.bag
```
or 
```sh
wget http://143.107.183.119:5380/datasets/volta_bloco_did_velodyne_septentrio_xsens.bag
```

### 4.2. Run the velodyne_curb_detector package

Open a terminal and execute the following command:

```sh
roslaunch velodyne_curb_detector velodyne_curb_detector2.launch
```

### 4.3. Run the rosbag 

Open another terminal and execute the following command (set properly the path to the bag file):
```sh
rosbag play volta_ambiental_velodyne_septentrio_xsens.bag
```
It will also open the ROS rviz to visualize the detection results.

### 4.4. Tunning the velodyne_curb_detector parameters

Open the velodyne_curb_detector2.launch file and adjust the parameters in the rosparam tag:
```sh
rosed velodyne_curb_detector velodyne_curb_detector2.launch
```
