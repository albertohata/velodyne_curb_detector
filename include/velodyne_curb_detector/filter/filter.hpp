/*
 * filter.hpp
 *
 *  Created on: 20/07/2014
 *      Author: Alberto Hata
 *
 * 20/07/14: Initial version
 *
 */

#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <tf/message_filter.h>
#include <sensor_msgs/PointCloud2.h>
#include <laser_geometry/laser_geometry.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <velodyne_pointcloud/point_types.h>
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/impl/transforms.hpp>
#include <visualization_msgs/Marker.h>
#include <TLinearFitter.h>
#include <velodyne_curb_detector/CurbParams.h>
#include <pcl_conversions/pcl_conversions.h>

#include <limits>

using namespace std;
using namespace pcl;

namespace velodyne_curb_detector {

  struct VRay{
    short number_points;
    double range;
//    double max_range, min_range;
    double x, y, z;
    uint16_t ring;
    float intensity;

    VRay() {
      number_points = 0;
      range = 0;
//      max_range = -numeric_limits<double>::max(); 
//      min_range =  numeric_limits<double>::max(); 
      x = y = z = 0;
      ring = 0;
      intensity = 0;
    };

  };

  // Point cloud types
  typedef pcl::PointCloud<pcl::PointXYZ> PclXYZType;
  typedef pcl::PointCloud<pcl::PointXYZRGB> PclRGBType;
  typedef pcl::PointCloud<pcl::PointXYZI> PclIntensityType;
  typedef pcl::PointCloud<velodyne_pointcloud::PointXYZIR> VelodynePclType;
      
  // Circular grid used in the ring compression analysis
  typedef vector<vector<VRay> > CircularGridType;

  class VelodyneCurbDetectorFilter {

    public:
      VelodyneCurbDetectorFilter();
      VelodyneCurbDetectorFilter(int ring_max, double x_max, double y_max, double range_min, double range_max, double height_max, double curb_min_param, double curb_max_param, double slope_param, double reg_dist_param, double velodyne_height, string velodyne_frame, string base_frame);
      ~VelodyneCurbDetectorFilter();

      // Ring compression and gradient analysis method
      void ringCompressionFilter(VelodynePclType pclIn, PclXYZType &pclOut);
  
      // Regression filter method
      void regressionFilter(PclXYZType pclIn,  PclXYZType &pclOut);

      // method to clear variables
      void clearVariables(uint64_t stamp, string frame_id);

      void getRegressionCurves(visualization_msgs::Marker &curb_line_A, visualization_msgs::Marker &curb_line_B);
  
      void getRegressionParameters(CurbParams &curb_params);

    private:
      
      pcl::PointCloud<pcl::PointXYZ> curb_points_[2], curb_points_filt_;

      // Used to display the detected curb lines 
      visualization_msgs::Marker curb_line_A_, curb_line_B_;

      // Used to LTS fitting 
      TFormula *f_;
      TLinearFitter *lf_A_;
      TLinearFitter *lf_B_;
      TVectorD params_A_, params_B_;

      // Used to detect curbs
      VRay default_ray_;
      VelodynePclType pcl2Transformed_;
      CircularGridType defaultGrid_;
      double curb_max_threshold_[22], curb_min_threshold_[22], cell_sector_size_[22];

      // Curb detector parameter variables
      int ring_max_;
      double x_max_;
      double y_max_;
      double range_min_;
      double range_max_;
      double height_max_;
      double curb_min_param_;
      double curb_max_param_;
      double slope_param_;
      double reg_dist_param_;
      double angle_increment_;
      double number_rays_;
      float velodyne_height_;
      string velodyne_frame_, base_frame_;

      // compute gradient of two 3D points
      template<typename PointT>
      double get3DGradient(PointT p1, PointT p2);

      // method to convert point cloud to circular grid
      void pcl2CircularGrid(VelodynePclType pcl, CircularGridType &grid);

      void computeRegression(TLinearFitter fitterA, TLinearFitter fitterB);
  };



}

