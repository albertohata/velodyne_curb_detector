/*
 * velodyne_curb_detector.hpp
 *
 *  Created on: 16/07/2014
 *      Author: Alberto Hata
 *
 * 16/07/14: Initial version
 *
 */

//#include <ros/ros.h>
//#include <message_filters/subscriber.h>
//#include <tf/message_filter.h>
//#include <sensor_msgs/PointCloud2.h>
//#include <laser_geometry/laser_geometry.h>
//#include <pcl/point_types.h>
//#include <pcl/point_cloud.h>
//#include <velodyne_pointcloud/point_types.h>
//#include <pcl_ros/transforms.h>
//#include <pcl_ros/point_cloud.h>
//#include <pcl_ros/impl/transforms.hpp>
//#include <visualization_msgs/Marker.h>
//#include <TLinearFitter.h>
//#include <velodyne_curb_detector/CurbParams.h>
//#include <pcl_conversions/pcl_conversions.h>

#include <velodyne_curb_detector/filter/filter.hpp>

using namespace std;
using namespace pcl;

namespace velodyne_curb_detector {

//  typedef pcl::PointCloud<velodyne_pointcloud::PointXYZIR> VelodynePclType;
//  typedef pcl::PointCloud<pcl::PointXYZRGB> PclRGBType;
//  typedef pcl::PointCloud<pcl::PointXYZI> PclIntensityType;
//
//  struct VRay{
//    short number_points;
//    double range;
//    double x, y, z;
//    uint16_t ring;
//    float intensity;
//
//    VRay() {
//      number_points = 0;
//      range = 0;
//      x = y = z = 0;
//      ring = 0;
//      intensity = 0;
//    };
//
//  };

  class VelodyneCurbDetector {

    public:
      VelodyneCurbDetector();
      ~VelodyneCurbDetector();
//      pcl::PointCloud<pcl::PointXYZ> curb_points[2], curb_points_filt;

    private:
      ros::NodeHandle nh_;
      void velodyneCallback(const VelodynePclType::ConstPtr& pcl2Msg);
      ros::Subscriber pcl2_sub_;
      ros::Publisher marker_pub_;
      ros::Publisher pcl2_pub_, pcl_curb_A_pub_, pcl_curb_B_pub_, pcl_curb_filt_pub_, curbParam_pub_;

      VelodyneCurbDetectorFilter *detectorFilter_;

//      ros::Time globalStamp;

      tf::StampedTransform transform_;
      tf::TransformListener tf_listener_;
      
      // Curb detector parameter variables
      int ring_max_;
      double x_max_;
      double y_max_;
      double range_max_;
      double range_min_;
      double height_max_;
      double slope_param_;
      float velodyne_height_;
      double curb_max_param_;
      double curb_min_param_;
      double reg_dist_param_;
      string velodyne_frame_, base_frame_;

      // For LTS fitting
//      TLinearFitter *lf_A_;
//      TLinearFitter *lf_B_;
//      TVectorD params_A_, params_B_;

//      // Circular grid used in the ring compression analysis
//      typedef vector<vector<VRay> > CircularGridType;
//
//      // Point cloud types
//      typedef pcl::PointCloud<pcl::PointXYZ> PclXYZType;
//      typedef pcl::PointCloud<pcl::PointXYZRGB> PclRGBType;
//      typedef pcl::PointCloud<pcl::PointXYZI> PclIntensityType;
//      typedef pcl::PointCloud<velodyne_pointcloud::PointXYZIR> VelodynePclType;
//
//      // Used to display the detected curb lines 
      visualization_msgs::Marker curb_line_A_, curb_line_B_;
//
//      // Used to LTS fitting 
//      TFormula *f;
//      TLinearFitter *lf_A;
//      TLinearFitter *lf_B;
//      TVectorD params_A, params_B;
//
//      // Used to detect curbs
//      VRay default_ray_;
//      float velodyne_height;
//      VelodynePclType pcl2Transformed_;
//      string velodyne_frame_, base_frame_;
//      CircularGridType pclGrid_, defaultGrid_;
      velodyne_curb_detector::CurbParams curb_params_;
//      double curb_max_threshold[22], curb_min_threshold[22], cell_sector_size[22];
//
//      // Curb detector parameter variables
//      int ring_max;
//      double x_max;
//      double y_max;
//      double range_max;
//      double range_min;
//      double height_max;
//      double slope_param;
//      double curb_max_param;
//      double curb_min_param;
//      double reg_dist_param;
//      //static const double angle_increment = M_PI/360.0; //0.50 degrees
//      //static const double number_rays=2*M_PI / angle_increment;
//      double angle_increment;
//      double number_rays;

      // publish a single point
      void publishSinglePoint(uint64_t stamp, string frame_id);

//      // compute gradient of two 3D points
//      template<typename PointT>
//      double get3DGradient(PointT p1, PointT p2);
//
//      // method to clear variables
//      void clearVariables(uint64_t stamp, string frame_id);
//
//      // method to convert point cloud to circular grid
//      void pcl2CircularGrid(VelodynePclType pcl, double *cell_sector_size, CircularGridType &grid);
//
//      // Ring compression and gradient analysis method
//      template<typename PointT>
//      void ringCompressionFilter(CircularGridType grid, double *curb_max_threshold, PointCloud<PointT> &pclOut);
//  
//      // Regression filter method
//      template<typename PointT>
//      void regressionFilter(TVectorD paramListA, TVectorD paramListB,  PointCloud<PointT> pclIn,  PointCloud<PointT> &pclOut);
//
//      void publishRegressionCurves(TVectorD paramListA, TVectorD paramListB);
//  
//      void getRegressionParameters(TVectorD paramListA, TVectorD paramListB, CurbParams &curb_params);
//
//      void computeRegression(TLinearFitter fitterA, TLinearFitter fitterB, TVectorD &paramListA, TVectorD &paramListB);
  };

}

